import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";
import React from "react";
import { useDispatch, useMappedState } from "redux-react-hook";
import { IState, ITodo } from "store";

const mapState = (state: IState) => ({
  todos: state.todos
});

const Crud: React.FC = (): JSX.Element => {
  const [newTodo, setNewTodo] = React.useState<string>("");
  const { todos } = useMappedState(mapState);
  const dispatch = useDispatch();

  const handleChange = (e: any) => {
    setNewTodo(e.target.value);
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    setNewTodo("");
    dispatch({ type: "add todo", todo: newTodo });
  };

  const removeItemById = (id: string) => () => {
    dispatch({ type: "remove todo", id });
  };

  return (
    <main style={{ margin: "0 auto", width: "400px" }}>
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField
          label="Add Item"
          margin="normal"
          onChange={handleChange}
          value={newTodo}
        />
      </form>

      <List>
        {todos.map((item: ITodo, index: number) => (
          <ListItem key={index} role={undefined} dense button>
            <ListItemText primary={item.text} />
            <ListItemSecondaryAction>
              <IconButton aria-label="Delete" onClick={removeItemById(item.id)}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </main>
  );
};

export default Crud;
