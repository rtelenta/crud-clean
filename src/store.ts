import { createStore, Store } from "redux";
import domain from "crud-domain";

export interface ITodo {
  id: string;
  text: string;
  completed: boolean;
}

export interface IState {
  todos: Array<ITodo>;
}

const reducer = (state: IState | null | undefined, action: any) => {
  if (!state) {
    return null;
  }

  switch (action.type) {
    case "add todo": {
      const addItem = domain.add_item_use_case.execute({
        text: action.todo
      });

      if (!addItem) {
        alert("Min 1 char");
        return state;
      }

      return {
        ...state,
        todos: domain.get_all_items_use_case.execute()
      };
    }

    case "remove todo": {
      const removeItem = domain.remove_item_by_id_use_case.execute({
        id: action.id
      });

      if (!removeItem) {
        return state;
      }

      return {
        ...state,
        todos: domain.get_all_items_use_case.execute()
      };
    }

    default:
      return state;
  }
};

const initialValues = {
  todos: domain.get_all_items_use_case.execute()
};

export const store: Store = createStore(reducer, initialValues);
