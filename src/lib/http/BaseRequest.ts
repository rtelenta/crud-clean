import axios from "axios";

export interface IConfig {
  baseURL: string;
  headers?: any;
}

export default abstract class BaseRequest {
  protected instance: any;
  protected baseUrl: string;

  constructor(config: IConfig) {
    this.baseUrl = "https://pokeapi.co/api/v2/";
    this.instance = axios.create(config);
  }

  protected abstract call_api(): Promise<any>;
}
